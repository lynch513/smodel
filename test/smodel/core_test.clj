(ns smodel.core-test
  (:require [clojure.test               :refer [testing is deftest use-fixtures]]
            [clojure.spec.alpha         :as s]
            [clojure.spec.test.alpha :as stest]
            ;; [clojure.spec.gen.alpha   :as gen]
            [smodel.core                :refer [get-page make-menu get-menu-item get-site add-path-to-page]]
            [smodel.model-test          :as model]
            [smodel.model-test-2        :as model2]))

(deftest test-get-page-fn
  (let [t-index-page     {:title       "Index page"
                          :slug        "index"
                          :description "Index page from root"}
        t-main-page-a    {:title       "Page A"
                          :slug        "pageA"
                          :description "This is page A from main directory"}
        t-service-page-a {:title       "Service A"
                          :slug        "serviceA"
                          :description "This is service page A from services directory"}]
    (testing "Get page from site root"
      (is (=
           t-index-page
           (get-page model/site ["index"]))))
    (testing "Find first page in given path with same slug"
      (is (=
           t-main-page-a
           (get-page model/site ["main" "pageA"]))))
    (testing "Get nested page 2"
      (is (=
           t-service-page-a
           (get-page model/site ["services" "serviceA"]))))
    (testing "Return nil if page not found"
      (is (nil?
            (get-page model/site ["services" "serviceD"]))))
    (testing "Return nil if given path is nil"
      (is (nil?
            (get-page model/site nil))))
    (testing "Return nil if given path is empty"
      (is (nil?
            (get-page model/site []))))
    (testing "Return nil if given model is nil"
      (is (nil?
            (get-page nil ["main" "pageA"]))))
    (testing "Return nil if given model is empty"
      (is (nil?
            (get-page {} ["main" "pageA"]))))))

(deftest test-add-path-to-page-fn
  (let [t-subservice-a {:title "Subservice A"
                        :slug  "subserviceA"
                        :description "This is subservice page A in service A directory"} 
        t-subservice-a-with-path (merge t-subservice-a
                                        {:path ["services" "serviceB"]
                                         :uri ["services" "serviceB" "subserviceA"]})]
    (testing "Add path to page using a add-path-to-page function"
      (is (=
           t-subservice-a-with-path
           (add-path-to-page (get-page model/site ["services" "serviceB" "subserviceA"]) ["services" "serviceB"]))))
    (testing "Return nil if page not found"
      (is (nil?
            (add-path-to-page (get-page model/site ["services" "serviceB" "subserviceD"]) ["services" "serviceB"]))))))

(deftest test-get-site-fn
  (is (=
       {:title "This is site name"}
       (get-site model/site))))

(deftest test-make-menu-fn
  (let [t-menu-item-a            {:title "Page A"
                                  :slug  "pageA"
                                  :path  ["main"]
                                  :uri   ["main" "pageA"]}
        t-menu-item-b            {:title "Page B"
                                  :slug  "pageB"
                                  :path  ["main"]
                                  :uri   ["main" "pageB"]}
        t-menu-item-one-more-a   {:title "One more page A"
                                  :slug  "pageA"
                                  :path  ["main"]
                                  :uri   ["main" "pageA"]}
        t-menu-item-subservice-a {:title "Subservice A"
                                  :slug  "subserviceA"
                                  :path  ["services" "serviceB"]
                                  :uri   ["services" "serviceB" "subserviceA"]}
        t-menu-item-subservice-b {:title "Subservice B"
                                  :slug  "subserviceB"
                                  :path  ["services" "serviceB"]
                                  :uri   ["services" "serviceB" "subserviceB"]}]
    (testing "Local menu from main directory site. Local menu haven't :path field"
      (is (=
           [t-menu-item-a t-menu-item-b t-menu-item-one-more-a]
           (make-menu model/site ["main"]))))
    (testing "Menu from services directory site. Menu must have :path field"
      (is (=
           [t-menu-item-subservice-a t-menu-item-subservice-b]
           (make-menu model/site ["services" "serviceB"]))))
    (testing "Return nil if path not found"
      (is (nil?
            (make-menu model/site ["path" "not" "found"]))))
    (testing "Return nil if found page and this page haven't field :child"
      (is (nil?
            (make-menu model/site ["services" "serviceA"]))))
    (testing "Return nil if path is nil"
      (is (nil?
            (make-menu model/site nil)))))
  )

(deftest test-get-menu-item-fn
  (let [service-b {:title "Service B"
                   :slug "serviceB"
                   :path ["services"]
                   :uri ["services" "serviceB"]}]
    (testing "Return menu item from given menu"
      (is (=
           service-b
           (get-menu-item (make-menu model/site ["services"]) "serviceB"))))
    (testing "Return nil if menu item not found"
      (is (nil?
            (get-menu-item (make-menu model/site ["services"]) "serviceD"))))))

(deftest test-spec-and-types
  (use-fixtures :once (fn [f]
                        (stest/instrument `get-page)
                        (stest/instrument `get-site)
                        (stest/instrument `make-menu)
                        (stest/instrument `get-menu-item)
                        (stest/instrument `add-path-to-page)
                        (f)
                        (stest/unstrument `get-page)
                        (stest/instrument `get-site)
                        (stest/unstrument `make-menu)
                        (stest/unstrument `get-menu-item)
                        (stest/unstrument `add-path-to-page)))
  (testing "get-page function on wrong data structure"
    (is (false?
          (s/valid? :smodel.core/page (get-page model/site ["main"])))))
  (testing "get-page function on right data structure"
    (is (true?
          (s/valid? :smodel.core/page (get-page model2/site ["main"])))))
  (testing "get-site function on right data structure"
    (is (true?
          (s/valid? :smodel.core/map-without-child-field (get-site model/site)))))
  (testing "make-menu and get result"
    (->> (make-menu model/site ["services" "serviceB"])
         first
         (s/valid? :smodel.core/menu-item)))
  (testing "get-menu-item and get result"
    (is (true?
          (s/valid? :smodel.core/menu-item (get-menu-item (make-menu model/site ["services"]) "serviceB")))))
  (testing "add-path-to-page and get result"
    (is (true?
          (s/valid? :smodel.core/page (add-path-to-page (get-page model/site ["services" "serviceB"]) ["services"]))))))



