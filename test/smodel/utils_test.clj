(ns smodel.utils-test
  (:require [clojure.test :refer [testing is deftest]]
            [smodel.utils :refer [read-text-file read-markdown-file markdown-text]]))

(deftest test-read-text-file-fn
  (testing "Read text file and escape html characters"
    (is (=
         "&lt;h1&gt;this is test\n"
         (read-text-file "test/test.txt")))))

(deftest test-read-markdown-file-fn
  (testing "Reads markdown file, escapes html characters and converts markdown to html"
    (is (=
         "<h1>this is test &lt;h1&gt;</h1>"
         (read-markdown-file "test/test_2.md")))))

(deftest test-markdown-text-fn
  (testing "Escapes html characters and converts markdown to html"
    (is (=
         "<h1>this is test &lt;h1&gt;</h1>"
         (markdown-text "# this is test <h1>")))))
