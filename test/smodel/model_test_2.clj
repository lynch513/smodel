(ns smodel.model-test-2)

;; This is test data structure with right data
(def site {:title "This is site name"
           :child [{:title       "Index page"
                    :slug        "index"
                    :description "Index page from root"}
                   {:title "Main"
                    :slug  "main"
                    :child [{:title       "Page A"
                             :slug        "pageA"
                             :description "This is page A from main directory"}
                            {:title       "Page B"
                             :slug        "pageB"
                             :description "This is page B from main directory"}]}
                   {:title "Services"
                    :slug "services"
                    :child [{:title       "Service A"
                             :slug        "serviceA"
                             :description "This is service page A from services directory"}
                            {:title       "Service B"
                             :slug        "serviceB"
                             :description "This is service page B from services directory"
                             :child [{:title "Subservice A"
                                      :slug  "subserviceA"
                                      :description "This is subservice page A in service A directory"}
                                     {:title "Subservice B"
                                      :slug  "subserviceB"
                                      :description "This is subservice page B in service B directory"}]}
                            {:title       "Service C"
                             :slug        "serviceC"
                             :description "This is service page C from services directory"}]}]})
