# smodel

A Clojure library for create simple static pages for site. This simple static model allows seek page or directory (smodel.core/get-page) and create menu
(smodel.core/make-menu). Also you can validate data structure and create page (smodel.core/make-page)"

## Usage

FIXME

## License

Copyright © 2018 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
