# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [0.1.0] - 2018-06-01
### Added
- Added types for page, site, menu, menu-item and others 
- Added macro make-page for checking types on compile time
- Added function get-page for seeking page in static data structure
- Added function make-menu for creating simple flat menu

## [0.1.1] - 2018-06-03
### Added 
- Added module smodel.utils and functions read-text-file and read-markdown-file

## [0.1.2] - 2018-06-05
### Deleted
- Removed smodel.model-test-2 from smodel.core

## [0.1.3] - 2018-06-05
### Removed
- Removed function make-page

### Added
- Added functions mk-page and mk-site for checking types page and site in runtime 
- Added macro defpage and defsite for defining vars and type checking in compile time

## [0.1.4] - 2018-06-05
### Changed

- Refactoring mk-page, mk-site and defpage, defsite

## [0.1.5] - 2018-06-05
### Changed 

- Refactoring mk-page, mk-site functions. Used spec/explain in functions preconditions for explaination type errors 

## [0.1.6] - 2018-06-06
### Added

- Added new function get-site. Returns root map from nested static pages structure

## [0.1.7] - 2018-06-09
### Added

- Added new function markdown-text. Convert to html, from given markdown text

## [0.1.8] - 2018-07-11
### Added

- Added new function get-menu-item in smodel.core. It finds and returns menu item from given menu.

## [0.1.9] - 2018-07-11
### Changed

- Changed one of main function get-page in smodel.core module. Now it returns map with :path and :uri fields. 

## [0.1.10] - 2018-07-14
### Added 

- Added function add-path-to-page in smodel.core module. 

### Changed

- Changed function get-page in smodel.core module. It returns pure page without :path and :uri fields. But if you need this fields in page
use add-path-to-page function
