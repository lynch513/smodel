(defproject org.clojars.lynch/smodel "0.1.10-SNAPSHOT"
  :description "Library for create simple static model of pages for web sites and others"
  :url "https://bitbucket.org/lynch513/smodel"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [hiccup              "1.0.5"]
                 [markdown-clj        "1.0.1"]]
  :main ^:skip-aot smodel.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev {:dependencies [[org.clojure/test.check "0.9.0"]]}})
