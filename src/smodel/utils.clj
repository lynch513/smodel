(ns smodel.utils
  (:require [clojure.java.io :as io]
            [markdown.core   :refer [md-to-html-string]]
            [hiccup.util     :refer [escape-html]]))

(defn read-resource-if-exist 
  "Opens and reads given file-name, returning a string. Throw exception
   java.io.FileNotFoundException if file doesn't exist"
  [file-name]
  (if-let [file-resource (io/resource file-name)]
    (slurp file-resource)
    (throw (java.io.FileNotFoundException. (format "file %s not found" file-name)))))

(defn read-text-file
  [file-name]
  "Reads given file-name and return as string. If resource doesn't exist throw
  exception. Escape html characters from text."
  (-> file-name
      read-resource-if-exist
      escape-html))

(defn read-markdown-file
  [file-name]
  "Reads given file-name, escape html characters, convert markdown to html
  and return as string. If resource doesn't exist throw exception."
  (-> file-name
      read-resource-if-exist
      escape-html
      md-to-html-string))

(defn markdown-text
  [markdown-text]
  "Returns html text converted from markdown. Escape html characters in source
  markdown text"
  (-> markdown-text
      escape-html
      md-to-html-string))

(comment 
  (read-resource-if-exist "test/test.txt")
  (read-text-file "test/description.txt")
  (read-text-file "test/test.txt")
  (md-to-html-string "* <h1>This is test")
  (read-resource-if-exist "main/index.txt")
  (read-markdown-file "main/content.md")
  )

