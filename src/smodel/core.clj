(ns smodel.core
  (:require [clojure.spec.alpha :as s]
            ;; [clojure.spec.test.alpha :as stest]
            ;; [clojure.spec.gen.alpha :as gen]
            ;; [smodel.model-test-2 :refer [site]]
            ))

(comment 
  (ns-unmap (find-ns 'smodel.core) 'site)
  (ns-unalias (find-ns 'smodel.core) 's)
  )

(s/def ::title string?)
(s/def ::slug string?)
(s/def ::path (s/coll-of ::slug :kind vector? into []))
(s/def ::uri (s/coll-of ::slug :kind vector? into []))

(s/def ::child (s/and 
                 (s/coll-of ::page :distinct? true :kind vector? :into [])
                 #(if (empty? %) true (apply distinct? (mapv :slug %)))))

(s/def ::page (s/keys :req-un [::title ::slug]
                      :opt-un [::child ::uri ::path]))

(s/def ::site (s/keys :req-un [::title ::child]))

(s/def ::model (s/or :site ::site :page ::page))

(s/def ::menu-item (s/keys :req-un [::title ::slug ::path ::uri]))

(s/def ::menu (s/+ ::menu-item))

(s/def ::map-without-child-field #(not (contains? % :child)))

;; (defn- check-type
;;   [t v]
;;   (if (s/valid? t v)
;;     v
;;     (IllegalArgumentException. (s/explain t v))))

(defn mk-page
  "Gets page as nested structure of map, check it types and return page. If type checking
  not pass throw exception."
  [page]
  {:pre [(or (s/valid? ::page page) (s/explain ::page page))]}
  page)

(defn mk-site
  "Gets site as nested structure of map, check it types and return site. If type checking
  not pass throw exception."
  [site]
  {:pre [(or (s/valid? ::site site) (s/explain ::site site))]}
  site)

(defmacro defpage
  "Gets name and page and define new var name with content that mk-page constructor returns.
  Types is checking in compile type. This macro can throw exception if type not valid."
  [name page]
  `(def ~name ~(mk-page page)))

(defmacro defsite
  "Gets name and site and define new var name with content that mk-site constructor returns.
  Types is checking in compile type. This macro can throw exception if type not valid."
  [name site]
  `(def ~name ~(mk-site site))
  )

(comment (if (s/valid? ::site site)
     `(def ~name ~site)
     (IllegalArgumentException. (s/explain ::site site))))

(comment
  (macroexpand-1 '(defpage test-page {:title2 "Test page" :slug "test-page"}))
  (macroexpand-1 '(defsite test-site {:title "Test site" :slug "site"}))
  (defsite test-site {:title "Test site" :slug "site"})
  (identity test-site)
  (identity test-site-2)
  test-site
  (def a 2)
  (identity a)
  )

(defn add-path-to-page
  "Returns page with added path and uri fields. Field path is given argument, field uri is
  concantenation of given path and page slug. Slug is value taken from given page argument"
  [{:keys [slug] :as page} path]
  (when page (-> page
                 (assoc :path path)
                 (assoc :uri (conj path slug)))))

(comment (add-path-to-page {:title "Test page" :slug "test"} ["main" "services"]))

(s/fdef add-path-to-page
        :args (s/cat :page ::page
                     :path ::path)
        :ret ::page)

(defn get-page
  "Finds and returns page from model. Page is a map with :title, :slug and others fields.
  Arg model is a nested structure of pages. Arg path is a vector of slugs with use for 
  search page in nested structure. Returns nil if page not found"
  [model path]
  (when-let [pages (:child model)]
    (loop [pages-in pages
           path-in  path]
      (let [slug (first path-in)
            rest-path (rest path-in)
            page (first (filter #(= (:slug %) slug) pages-in))]
        (when (some? page)
          (if (empty? rest-path)
            page 
            (recur
              (:child page)
              rest-path)))))))

(s/fdef get-page
  :args (s/cat :model ::model
               :path  ::path)
  :ret ::page)

(defn get-site
  "Returns root map from nested static page structure"
  [model]
  (dissoc model :child))

(s/fdef get-site
  :args (s/cat :model ::site)
  :ret ::map-without-child-field)

(comment
 (stest/check `get-page {:num-tests 1})
 (gen/generate (s/gen ::page))
 (gen/generate (s/gen ::child))
 (gen/generate (s/gen ::site))
 )

(defn make-menu
  "Returns menu list from given path in model. Find page in nested pages structure and make
  simple menu list from pages in :child vector. Returns nil if page not found or if searched
  page haven't :child field"
  [model path]
  (when-let [pages (:child (get-page model path))]
    (map (fn [{:keys [slug] :as page}]
           (-> (select-keys page [:title :slug :path :uri])
               (assoc :slug slug)
               (assoc :path path)
               (assoc :uri (conj path slug))))
         pages)))

(s/fdef make-menu
  :args (s/cat :model ::model 
               :path  (s/? ::path))
  :ret ::menu)

(defn get-menu-item
  "Returns menu item from given path in menu. Find menu item in flat menu items sequence.
  Returns nil if menu item not found"
  [model slug]
  (first 
    (filter 
      #(= (:slug %) slug)
      model)))

(s/fdef get-menu-item
  :args (s/cat :model ::menu
               :slug ::slug)
  :ret  ::menu-item)

(comment
  (identity site)
  (make-page {:title "Test page" :slug2 "testA"})
  (s/valid? ::page {:title (identity "Test page") :slug "testA"})
  (make-page {:title "Test page" :slug "testA"})
  (get-page site ["index"])
  (get-page site ["main" "pageA"])
  (get-page (get-page site ["services" "serviceA"]) ["serviceD"])
  (get-page site ["services" "serviceA"])
  (get-page site ["services" "serviceB"])
  (get-page site ["services" "serviceB" "subserviceA"])
  (get-page site ["services" "serviceD"])
  (get-menu-item (make-menu site ["services"]) "serviceB")
  (make-menu site ["main"])
  (make-menu site [])
  )

(comment
  (->> (get-page site ["services"])
       (s/valid? ::page)
       )
  (s/valid? ::page {:title "Test" :slug "test" :b "c"})
  (s/valid? ::page {:title "Test page"
                    :slug "test-page"
                    :child [{:title "SubsectionA" :slug "subsectionA"}
                            {:title "SubsectionA" :slug "subsectionA"}]})
  (get-page site ["services" "serviceA"])
  (make-menu site ["services" "serviceB"])
  (make-menu site ["main"])
  (->> (make-menu site ["services" "serviceB"])
       first
       (s/valid? ::menu-item))
  (->> (make-menu site ["services" "serviceB"])
       (s/valid? ::menu))
  )


